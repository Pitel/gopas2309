package cz.gopas.kalkulacka

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentContainerView
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController

class MainActivity : AppCompatActivity(R.layout.activity_main) {
//    private val aText: TextInputEditText by lazy { findViewById(R.id.a_text) }
//    private val bText: TextInputEditText by lazy { findViewById(R.id.b_text) }
//    private val ops: RadioGroup by lazy { findViewById(R.id.ops) }
//    private val res: MaterialTextView by lazy { findViewById(R.id.res) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (BuildConfig.DEBUG) {
            Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "Create")
        }

//        if (savedInstanceState == null) {
//            supportFragmentManager.commit {
//                replace(R.id.container, CalcFragment())
//                // addToBackStack(null)
//            }
//        }

        val navController = findViewById<FragmentContainerView>(R.id.container)
            .getFragment<NavHostFragment>().navController
        setupActionBarWithNavController(
            navController,
            AppBarConfiguration(navController.graph)
        )

        Log.d(TAG, "${intent.dataString}")
    }

    override fun onSupportNavigateUp() =
        findNavController(R.id.container).navigateUp() || super.onSupportNavigateUp()

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Resume")
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Pause")
    }

//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        outState.putCharSequence(RES_KEY, res.text)
//    }
//
//    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
//        super.onRestoreInstanceState(savedInstanceState)
//        res.text = savedInstanceState.getCharSequence(RES_KEY)
//    }

    private companion object {
        private val TAG = MainActivity::class.java.simpleName
        private const val RES_KEY = "res"
    }
}
