package cz.gopas.kalkulacka.history

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.room.Room
import kotlinx.coroutines.flow.MutableStateFlow

class HistoryViewModel(app: Application) : AndroidViewModel(app) {
    val clicked = MutableStateFlow<Float?>(null)

    val db = Room.databaseBuilder(app, HistoryDatabase::class.java, "history")
//        .allowMainThreadQueries()
        .build().historyDao()
}
