package cz.gopas.kalkulacka.history

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import cz.gopas.kalkulacka.R
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class HistoryFragment : Fragment(R.layout.fragment_history) {

    private val historyViewModel by viewModels<HistoryViewModel>({ requireActivity() })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recycler = view as RecyclerView
        recycler.setHasFixedSize(true)
        val adapter = HistoryAdapter {
            historyViewModel.clicked.value = it
            findNavController().popBackStack()
        }
        recycler.adapter = adapter

//        adapter.submitList(List(100_000) { HistoryEntity(it.toFloat(), it.toLong()) })
        historyViewModel.db.getAll()
            .onEach { adapter.submitList(it) }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }
}
