package cz.gopas.kalkulacka.calc

import android.app.Application
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.floatPreferencesKey
import androidx.lifecycle.AndroidViewModel
import cz.gopas.kalkulacka.App.Companion.dataStore
import cz.gopas.kalkulacka.R
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map

class CalcViewModel(private val app: Application) : AndroidViewModel(app) {
    private val _result = MutableStateFlow<Float?>(null)
    val result: StateFlow<Float?> = _result

//    private val prefs = PreferenceManager.getDefaultSharedPreferences(app)
//    var ans: Float
//        get() = prefs.getFloat(ANS_KEY, 0f)
//        private set(value) = prefs.edit {
//            putFloat(ANS_KEY, value)
//        }

    fun getAns() = app.dataStore.data.map {
        it[ANS_DATA_KEY]
    }

    suspend fun saveAns(ans: Float) {
        app.dataStore.edit {
            it[ANS_DATA_KEY] = ans
        }
    }

    suspend fun calc(a: Float, b: Float, @IdRes op: Int) {
        _result.value = when (op) {
            R.id.add -> a + b
            R.id.sub -> a - b
            R.id.mul -> a * b
            R.id.div -> a / b
            else -> Float.NaN
        }.also {
            saveAns(it)
        }
    }

    private companion object {
        private const val ANS_KEY = "ans"
        private val ANS_DATA_KEY = floatPreferencesKey(ANS_KEY)
    }
}
