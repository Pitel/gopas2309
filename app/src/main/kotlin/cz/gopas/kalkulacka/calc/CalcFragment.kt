package cz.gopas.kalkulacka.calc

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import cz.gopas.kalkulacka.R
import cz.gopas.kalkulacka.databinding.FragmentCalcBinding
import cz.gopas.kalkulacka.history.HistoryEntity
import cz.gopas.kalkulacka.history.HistoryViewModel
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class CalcFragment : Fragment(), MenuProvider {

    private var binding: FragmentCalcBinding? = null

    private val viewModel by viewModels<CalcViewModel>()
    private val historyViewModel by viewModels<HistoryViewModel>({ requireActivity() })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCalcBinding.inflate(inflater, container, false)
        .also { binding = it }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.calc?.setOnClickListener {
            Log.d(TAG, "${it.id}")
            viewLifecycleOwner.lifecycleScope.launch {
                calc()
            }
        }
        binding?.share?.setOnClickListener {
            startActivity(
                Intent.createChooser(
                    Intent(Intent.ACTION_SEND)
                        .setType("text/plain")
                        .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, binding?.res?.text)),
                    null
                )
            )
        }
        binding?.ans?.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                Log.d(TAG, "Ans clicked")
                viewModel.getAns().first()?.let {
                    Log.d(TAG, "Got ans $it")
                    binding?.bText?.setText("$it")
                }
                Log.d(TAG, "Ans done")
            }
        }

        binding?.history?.setOnClickListener {
            findNavController().navigate(CalcFragmentDirections.history())
        }

        requireActivity().addMenuProvider(this, viewLifecycleOwner)

        viewModel.result
            .filterNotNull()
            .onEach {
                Log.d(TAG, "$it")
                historyViewModel.db.insert(HistoryEntity(it))
            }
            .map { "$it" }
            .onEach {
                binding?.res?.text = it
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    override fun onStart() {
        super.onStart()
        historyViewModel.clicked.onEach {
            if (it != null) {
                binding?.aText?.setText("$it")
                historyViewModel.clicked.value = null
            }
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private suspend fun calc() {
        Log.d(TAG, "Calc!")
        val a = "${binding?.aText?.text}".toFloatOrNull() ?: Float.NaN
        val b = "${binding?.bText?.text}".toFloatOrNull() ?: Float.NaN
        viewModel.calc(a, b, binding!!.ops.checkedRadioButtonId)
//        binding?.res?.text = when (binding?.ops?.checkedRadioButtonId) {
//            R.id.add -> a + b
//            R.id.sub -> a - b
//            R.id.mul -> a * b
//            R.id.div -> {
//                if (b == 0f) {
//                    findNavController().navigate(CalcFragmentDirections.zero())
//                }
//                a / b
//            }
//            else -> Float.NaN
//        }.toString()
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.menu, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem) = when (menuItem.itemId) {
        R.id.about -> {
//            parentFragmentManager.commit {
//                replace(R.id.container, AboutFragment("Jan Kaláb"))
//                addToBackStack(null)
//            }
            findNavController().navigate(CalcFragmentDirections.about("Jan Kaláb"))
            true
        }
        else -> false
    }

    private companion object {
        private val TAG = CalcFragment::class.simpleName
    }
}
