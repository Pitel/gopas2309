package cz.gopas.kalkulacka.calc

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import cz.gopas.kalkulacka.R

class ZeroDialog : DialogFragment() {

//    init {
//        isCancelable = false
//    }

    override fun onCreateDialog(savedInstanceState: Bundle?) = MaterialAlertDialogBuilder(requireContext())
        .setMessage(R.string.zero_div)
        .create()
}
