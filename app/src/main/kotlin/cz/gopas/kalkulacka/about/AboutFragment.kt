package cz.gopas.kalkulacka.about

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.google.android.material.textview.MaterialTextView
import cz.gopas.kalkulacka.R

class AboutFragment : Fragment(R.layout.fragment_about) {
//    constructor(name: String) : this() {
//        arguments = bundleOf(NAME_KEY to name)
//    }

    private val args by navArgs<AboutFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (view as MaterialTextView).text = getString(
            R.string.about_text,
            args.name
        )
    }

    private companion object {
        private const val NAME_KEY = "name"
    }
}
